package log

import (
    "testing"
    //"time"
    //"fmt"
)

/*func TestPrint(t *testing.T) {
    Log("Test")
    time.Sleep(time.Second)
    Log("Test 1")
}

func TestPrint2(t *testing.T) {
    Log("Test 2")
}

func TestPrint3(t *testing.T) {
    LogInfo("Critical 1")
    SetLevel(Warning)
    Log("Test 2")
    LogError("OH NO!!!")
    LogInfo("Critical 2")
}*/

func TestInt(t *testing.T) {
    //Log("This should go to a file")
    SetOut(OutConsole)
    Log("This should go to the console", 1234)
    //SetOut(OutConsole | OutFile)
    //Log("This should go to file and console")
}


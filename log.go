package log

import (
    "time"
    "fmt"
    "os"
)



type Logger struct {
    filename string
    out int
}

type Level int

const (
    None Level = iota
    Undefined
    Panic
    Error
    Warning
    Info
    Debug
)

const (
    Log_All = Undefined | Panic | Error | Warning | Info | Debug
)

const (
    OutNone = iota
    OutFile
    OutConsole
)

const (
    prefixUndefined = ""
    prefixError = "ERROR"
    prefixWarning = "WARNING"
    prefixInfo = "INFO"
    prefixDebug = "DEBUG"
    prefixPanic = "PANIC"
)

var logger *Logger
var lvl Level

func init() {
    fileName := fmt.Sprintf("%s__logfile.log", time.Now().Format("2006_01_02_15_04_05"))
    logger = &Logger{filename: fileName, out: OutFile}
    lvl = Log_All
} 

func SetLevel(level Level) {
    lvl = level
}

func SetFileName(name string) {
    fileName := fmt.Sprintf("%s__%s.log", time.Now().Format("2006_01_02_15_04_05"), name)
    logger.filename = fileName
}

func SetOut(o int) {
    logger.out = o
}

func LogPanic(v error) {
    if lvl & Panic == Panic {
        doLog(prefixPanic, v)
        panic(v)
    }
}

func Log(v ...interface{}) {
    doLog(prefixUndefined, v)
}


func LogError(v ...interface{}) {
    if lvl & Undefined == Undefined {
        doLog(prefixError, v)
    }
}

func LogWarning(v ...interface{}) {
    if lvl & Error == Error {
        doLog(prefixWarning, v)
    }
}

func LogInfo(v ...interface{}) {
    if lvl & Warning == Warning {
        doLog(prefixInfo, v)
    }
}

func LogDebug(v ...interface{}) {
    if lvl & Info == Info {
        doLog(prefixDebug, v)
    }
}

func writeToFile(toLog string) {
    if logger.out & OutFile != OutFile {
        return
    }
    f, err := os.OpenFile(logger.filename, os.O_CREATE | os.O_APPEND | os.O_WRONLY, 0666)
    if err != nil {
        return
    }
    defer f.Close()
    
    f.WriteString(toLog)
    
}

func doLog(levelPrefix string, values ...interface{}) {
        toLog := getLogString(levelPrefix, values)
        writeToFile(toLog)
        writeToConsole(toLog)
}

func writeToConsole(toLog string) {
    if logger.out & OutConsole == OutConsole {
       fmt.Print(toLog)
    }
    
}

func getLogString(levelPrefix string, values ...interface{}) string {
    result := ""
    for _, value := range values {
        result = fmt.Sprintf("%s %v", result, value)
    }
    
    return fmt.Sprintf("%s\t %s >\t %s\n", levelPrefix, time.Now().Format("2006.01.02 15:04:05"), result)
}
